<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Run Apps</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>92796c87-f0d0-4d7a-b2aa-99aa67080f6f</testSuiteGuid>
   <testCaseLink>
      <guid>dbae57dc-d231-4b9d-99f5-5604bc6d362b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Open Apps</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>388f3270-debc-436e-8670-04f41e777dcd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC-01</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>911ac098-57c4-42ed-8ee3-6c8cf2e2ef93</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3e0e7a5c-c84d-42b4-84e3-a2515d88596f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2bf4a6d9-234a-46dd-9597-e74fab805137</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC-02</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>59d98e2b-6fce-462a-ac44-0dfd1451f248</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC-03</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1ed29d1f-3ad5-45af-a94b-181239c2c526</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC-04</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab4db67e-4b31-4736-b613-c5ceac4a7ead</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC-05</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
