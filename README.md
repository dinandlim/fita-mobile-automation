# Run Mobile Automation Testing with Katalon Studio on Windows

## Getting started

Before running mobile automation testing in Katalon Studio, we need do some installation process first

You can follow the step bellow or just follow the tutorial from katalon site in [here](https://docs.katalon.com/katalon-studio/videos/intro_mobile_app_testing.html)

## Installation

- Please Install [Katalon Studio](https://www.katalon.com/download/)
- Please Install [Node.js](https://nodejs.org/en/download/)
- Please Install Appium in cmd : npm install -g appium

## Run Mobile Automation

- Step 1 : Check if Node.js is already installed node -v node –version npm -v npm –version
- Step 2 : Check if appium is installed appium –version appium -v where appium
- Step 3 : Set adb.exe path in environment variable for user path (\.katalon\tools\android_sdk\platform-tools)
- Step 4: Open Katalon Studio and connect to Appium
- Step 5: Setup android mobile device for automation
- [ ] Settings – About Phone – tap 7 times on Build number
- [ ] Settings – Developer Options – enable USB Debugging
- [ ] If failed please change USB debugging into PTP
Step 6: Connect android device to your system (usb cable)
Step 7: Run the project in testsuite "Run Apps"

## Setup Structure Automation to fit SDLC

- [ ] Determine the scope for automation testing
- [ ] Cover Test Plan and Test Design
- [ ] Setting for test environment
- [ ] Run Test for cover Regression Testing
