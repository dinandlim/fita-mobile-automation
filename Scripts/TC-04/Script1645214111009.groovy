import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.lang.Math as Math

int clothingPrice = Integer.parseInt(GlobalVariable.clothingPrice)

int accessoriesPrice = Integer.parseInt(GlobalVariable.accessoriesPrice)

int shippingcost = 7

int item = 2

double subTotal = clothingPrice + accessoriesPrice

double totalShippingCost = shippingcost * item

double tax = (subTotal * 6) / 100

double totalPrice = (subTotal + totalShippingCost) + tax

String calculateTotPrice = Double.toString(totalPrice)

Mobile.waitForElementPresent(findTestObject('Cart Page/Text Total Purchase'), 0)

String strTotalPrice = Mobile.getAttribute(findTestObject('Cart Page/Text Total Purchase'), 'contentDescription', 0)

String[] parts = strTotalPrice.split('\\$')

String TotalPrice = parts[1]

Mobile.verifyEqual(calculateTotPrice, TotalPrice)



