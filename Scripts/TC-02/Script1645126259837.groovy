import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.ArrayList as ArrayList

Mobile.waitForElementPresent(findTestObject('Shrine Page/Button Hamburger'), 0)

Mobile.tap(findTestObject('Shrine Page/Button Hamburger'), 0)

Mobile.waitForElementPresent(findTestObject('Menu Page/Button Clothing'), 0)

Mobile.tapAndHold(findTestObject('Menu Page/Button Clothing'), 1, 0)

Mobile.doubleTap(findTestObject('Shrine Page/Button Hamburger'), 0)

deviceHeight = Mobile.getDeviceHeight()

deviceWidth = Mobile.getDeviceWidth()

int startY = deviceHeight * 0.2

int endY = startY

int startX = deviceWidth * 0.60

int endX = deviceWidth * 0.40

while (Mobile.waitForElementNotPresent(findTestObject('Shrine Page/Button Clothing Choice'), 1) == true) {
    Mobile.swipe(startX, startY, endX, endY)
}

Mobile.waitForElementPresent(findTestObject('Shrine Page/Button Clothing Choice'), 0)

String contentdesc = Mobile.getAttribute(findTestObject('Shrine Page/Button Clothing Choice'), 'contentDescription', 0)

Mobile.tapAndHold(findTestObject('Shrine Page/Button Clothing Choice'), 0, 0)

String[] parts1 = contentdesc.split(',')

String partprice = parts1[0]

String[] parts2 = partprice.split('\\$')

String strPriceClothing = parts2[1]

GlobalVariable.clothingPrice = strPriceClothing


