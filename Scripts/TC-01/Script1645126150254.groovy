import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.waitForElementPresent(findTestObject('Landing Page/Reply Button'), 0)

deviceHeight = Mobile.getDeviceHeight()

deviceWidth = Mobile.getDeviceWidth()

int startY = deviceHeight * 0.2

int endY = startY

int startX = deviceWidth * 0.70

int endX = deviceWidth * 0.30

Mobile.swipe(startX, startY, endX, endY)

Mobile.waitForElementPresent(findTestObject('Landing Page/Shrine Button'), 0)

Mobile.tapAndHold(findTestObject('Landing Page/Shrine Button'), 1, 0)

Mobile.waitForElementPresent(findTestObject('Login Page/Textfield Username'), 0)

Mobile.tap(findTestObject('Login Page/Textfield Username'), 0)

Mobile.sendKeys(findTestObject('Login Page/Textfield Username'), username, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Login Page/Textfield Password'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.sendKeys(findTestObject('Login Page/Textfield Password'), password, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Login Page/Button Next'), 0)

